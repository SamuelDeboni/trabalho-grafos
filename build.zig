const std = @import("std");
const Builder = std.build.Builder;

pub fn build(b: *Builder) void {
    var target = b.standardTargetOptions(.{});
    // target.abi = .musl;

    const mode = b.standardReleaseOptions();
    const strip = b.option(bool, "strip", "strip debug info") orelse false;

    var exe = b.addExecutable("trabalho_grafos", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    if (strip)
        exe.strip = true;

    exe.linkSystemLibrary("X11");
    exe.linkSystemLibrary("c");
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
