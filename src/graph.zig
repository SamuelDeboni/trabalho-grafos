//! Esse arquivo é onde se encontra os algoritimos

usingnamespace @import("util.zig");
const std = @import("std");
const math = std.math;
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const fs = std.fs;
const time = std.time;

const draw = @import("pixel_draw.zig");
usingnamespace draw.vector_math;

// Fonte de texto
pub var font: draw.BitmapFont = undefined;
pub var font_small: draw.BitmapFont = undefined;

// Aeroportos selecionados
pub var selected_aeroport_1: i32 = -1;
pub var selected_aeroport_2: i32 = -1;

// Estrutura que representa um aeroporto
pub const Aeroport = struct {
    id: u32 = 0, // Id do aeroporto
    name: [3]u8, // nome do aeroporto
    coords: [2]f32, // coordenadas do aeroporto
    screen_pos: [2]u32, // posição na tela do aeroporto
};

// Grafo de aeroportos
pub const AeroportGraph = struct {
    aeroports: []Aeroport, // Lista de aeroportos
    flights: []i32, // Matriz de preco
    flights_dist: []i32, // Matriz de distancia
    flights_height: []i32, // Matriz de altitude
    size: u32 = 0, // numero de vertices
};

/// Funcao que limpa a memoria de um grafo
pub fn freeAeroportGraph(al: *Allocator, graph: *AeroportGraph) void {
    al.free(graph.flights);
    al.free(graph.flights_dist);
    al.free(graph.flights_height);
    al.free(graph.aeroports);
}

/// Transform latitude and longitudo to screen space
pub fn coordinatesToScreenSpace(latitude: f32, longitude: f32) [2]u32 {
    const x = @floatToInt(u32, 1280 * (longitude + 180.0) / 360.0);
    const y = @floatToInt(u32, 720 * (-latitude + 90.0) / 180.0);
    return .{ x, y };
}

/// Cria um aeroporto
pub fn createAeroport(id: u32, name: []const u8, lat: f32, long: f32) Aeroport {
    assert(name.len >= 3);
    return Aeroport{
        .id = id,
        .name = .{ name[0], name[1], name[2] },
        .coords = .{ lat, long },
        .screen_pos = coordinatesToScreenSpace(lat, long),
    };
}

/// Le o arquivo de aeroportos e retorna uma lista
pub fn readAeroportListAlloc(al: *Allocator, path: []const u8) ![]Aeroport {
    const cwd = fs.cwd();
    var file_data = try cwd.readFileAlloc(al, path, std.math.maxInt(u32));
    defer al.free(file_data);
    var data = file_data;
    
    var line = nextLineSlice(&data);
    
    removeLeadingSpaces(&line);
    removeTrailingSpaces(&line);
    const count = try std.fmt.parseInt(u32, line, 10);
    var result = try al.alloc(Aeroport, count);
    
    var i: u32 = 0;
    while (data.len > 0 and i < count) {
        line = nextLineSlice(&data);
        if (line.len == 0) continue;
        
        removeLeadingSpaces(&line);
        removeTrailingSpaces(&line);
        
        var name = getToken(&line, ' ');
        var lat = try std.fmt.parseFloat(f32, getToken(&line, ' '));
        var long = try std.fmt.parseFloat(f32, getToken(&line, ' '));
        
        result[i] = createAeroport(i, name, lat, long);
        
        i += 1;
    }
    
    return result;
}

/// Desenha um aeroporto
pub fn drawAeroport(aeroport: Aeroport) void {
    var color = Color.c(0.7, 0.7, 0.7, 1.0);
    
    if (selected_aeroport_1 == aeroport.id) {
        color = Color.c(0.0, 0.39, 1.0, 1.0);
    } else if (selected_aeroport_2 == aeroport.id) {
        color = Color.c(1.0, 0.36, 0.0, 1.0);
    }
    
    draw.gb.fillCircle(@intCast(i32, aeroport.screen_pos[0]), @intCast(i32, aeroport.screen_pos[1]), 15, color);
    
    { // Highlight if the mouse is on top
        const dx = @intToFloat(f32, draw.mouse_pos_x - @intCast(i32, aeroport.screen_pos[0]));
        const dy = @intToFloat(f32, draw.mouse_pos_y - @intCast(i32, aeroport.screen_pos[1]));
        const dist = @sqrt(dx*dx + dy*dy);
        
        if (dist < 25) {
            draw.gb.fillCircle(@intCast(i32, aeroport.screen_pos[0]), @intCast(i32, aeroport.screen_pos[1]), 25, color);
        }
    }
    
    draw.gb.drawBitmapFontFmt("{s}", .{aeroport.name},
                              aeroport.screen_pos[0] - 12,
                              aeroport.screen_pos[1] - 5,
                              1, 1, font_small);
    
}

/// Desenha um voo
pub fn drawLineBetweenAeroports(a1: Aeroport, a2: Aeroport, cost: i32, dist: i32, height: i32) void {
    var color = Color.c(0.6, 0.6, 0.6, 1);
    var width: u32 = 2;
    
    if (cost == -1) {
        color = Color.c(1.0, 0.2, 0.2, 1);
        width = 3;
    }
    
    draw.gb.drawLineWidth(@intCast(i32, a1.screen_pos[0]), @intCast(i32, a1.screen_pos[1]),
                          @intCast(i32, a2.screen_pos[0]), @intCast(i32, a2.screen_pos[1]),
                          color, width);
    
    if (cost >= 0) {
        const px = (a1.screen_pos[0] + a2.screen_pos[0]) / 2;
        const py = (a1.screen_pos[1] + a2.screen_pos[1]) / 2;
        
        draw.gb.drawBitmapFontFmt("{}", .{cost}, px, py, 1, 1, font_small);
        draw.gb.drawBitmapFontFmt("{}:{}k", .{dist, @divFloor(height, 10)}, px - 5, py + 10, 1, 1, font_small);
        //draw.gb.drawBitmapFontFmt("{}", .{height}, px, py + 10, 1, 1, font_small);
    }
}

/// Le o arquivo de voos e aeroportos e cria um grafo
pub fn createAeroportGraphFromFile(al: *Allocator) !AeroportGraph {
    var aeroports = try readAeroportListAlloc(al, "aeroportos.txt");
    
    const aeroport_graph = AeroportGraph{
        .aeroports = aeroports,
        .flights = try al.alloc(i32, aeroports.len * aeroports.len),
        .flights_dist = try al.alloc(i32, aeroports.len * aeroports.len),
        .flights_height = try al.alloc(i32, aeroports.len * aeroports.len),
        .size = @intCast(u32, aeroports.len),
    };
    
    for (aeroport_graph.flights) |*it| it.* = 0;
    for (aeroport_graph.flights_dist) |*it| it.* = 0;
    for (aeroport_graph.flights_height) |*it| it.* = 0;
    
    // Setup flights
    const cwd = fs.cwd();
    var file_data = try cwd.readFileAlloc(al, "voos.txt", std.math.maxInt(u32));
    defer al.free(file_data);
    var data = file_data;
    
    var line = nextLineSlice(&data);
    
    removeLeadingSpaces(&line);
    removeTrailingSpaces(&line);
    const count = try std.fmt.parseInt(u32, line, 10);
    
    var i: u32 = 0;
    while (data.len > 0 and i < count) {
        line = nextLineSlice(&data);
        if (line.len == 0) continue;
        
        removeLeadingSpaces(&line);
        removeTrailingSpaces(&line);
        
        var name1 = getToken(&line, ' ');
        var name2 = getToken(&line, ' ');
        var price = try std.fmt.parseInt(i32, getToken(&line, ' '), 10);
        
        removeLeadingSpaces(&name1);
        removeTrailingSpaces(&name1);
        
        removeLeadingSpaces(&name2);
        removeTrailingSpaces(&name2);
        
        var index_name1: usize = 0;
        for (aeroport_graph.aeroports) |it, _i| {
            if (std.mem.eql(u8, &it.name, name1)) {
                index_name1 = _i;
                break;
            }
        }
        
        var index_name2: usize = 0;
        for (aeroport_graph.aeroports) |it, _i| {
            if (std.mem.eql(u8, &it.name, name2)) {
                index_name2 = _i;
                break;
            }
        }
        
        aeroport_graph.flights[index_name1 + index_name2 * aeroport_graph.size] = price;
        aeroport_graph.flights[index_name2 + index_name1 * aeroport_graph.size] = price;
        
        const dist_x = aeroport_graph.aeroports[index_name2].coords[0] -
            aeroport_graph.aeroports[index_name1].coords[0];
        
        const dist_y = aeroport_graph.aeroports[index_name2].coords[1] -
            aeroport_graph.aeroports[index_name1].coords[1];
        
        const dist = @floatToInt(i32, @sqrt(dist_x*dist_x + dist_y*dist_y));
        
        aeroport_graph.flights_dist[index_name1 + index_name2 * aeroport_graph.size] = dist;
        aeroport_graph.flights_dist[index_name2 + index_name1 * aeroport_graph.size] = dist;
        
        aeroport_graph.flights_height[index_name1 + index_name2 * aeroport_graph.size] = 100;
        aeroport_graph.flights_height[index_name2 + index_name1 * aeroport_graph.size] = 100;
        
        i += 1;
    }
    return aeroport_graph;
}


/// Algoritimo de menor caminho
pub fn shortestPath(al: *Allocator, graph: AeroportGraph,
                    start: u32, end: u32, use_dist: bool) void
{
    const init_time = time.nanoTimestamp();
    
    // lista de distancia
    var distance = al.alloc(i32, graph.size) catch unreachable;
    defer al.free(distance);
    
    // lista de origem
    var origin = al.alloc(i32, graph.size) catch unreachable;
    defer al.free(origin);
    
    // se ja foi visitado
    var is_set = al.alloc(bool, graph.size) catch unreachable;
    defer al.free(is_set);
    
    // inicializar lista
    var i: u32 = 0;
    while (i < graph.size) : (i += 1) {
        distance[i] = std.math.maxInt(i32);
        is_set[i] = false;
        origin[i] = -1;
    }
    
    // distancia do comeco e 0
    distance[start] = 0;
    
    // Achar o menor caminho para todos os vertices
    var count: u32 = 0;
    while (count < graph.size - 1) : (count += 1) {
        
        // Achar visinho mais proximo
        var u = blk: {
            var min: i32 = std.math.maxInt(i32);
            var min_index: u32 = undefined;
            
            var v: u32 = 0;
            while (v < graph.size) : (v += 1) {
                if (is_set[v] == false and distance[v] <= min) {
                    min = distance[v];
                    min_index = v;
                }
            }
            
            break :blk min_index;
        };
        
        // u ja foi visitado
        is_set[u] = true;
        
        // calcular distancia
        var v: u32 = 0;
        while (v < graph.size) : (v += 1) {
            const dist = if (use_dist) graph.flights_dist[u * graph.size + v] else graph.flights[u * graph.size + v];
            
            if (!is_set[v] and dist > 0 and
                distance[u] != std.math.maxInt(i32) and
                distance[u] + dist < distance[v])
            {
                distance[v] = distance[u] + dist;
                origin[v] = @intCast(i32, u);
            }
        }
    }
    
    const end_time = time.nanoTimestamp();
    std.debug.print("shortest path t = {}\n", .{end_time - init_time});
    
    
    // Desenhar o resultado
    i = end;
    while (i != start) {
        const pi = @intCast(u32, origin[i]);
        drawLineBetweenAeroports(graph.aeroports[i], graph.aeroports[pi], -1, 0, 0);
        i = pi;
    }
    
    if (use_dist) {
        draw.gb.drawBitmapFontFmt("Distancia Total = {}",.{distance[end]},
                                  10, draw.gb.height - 30, 2, 2, font_small);
    } else {
        draw.gb.drawBitmapFontFmt("Preco Total = {}",.{distance[end]},
                                  10, draw.gb.height - 30, 2, 2, font_small);
    }
}

/// Algoritimo de volta ao mundo usando visinho mais proximo
pub fn worldTour(al: *Allocator, graph: AeroportGraph, start: u32) struct {price: i32, path: []u32} {
    const init_time = time.nanoTimestamp();
    
    // Lista de visitados
    var visited = al.alloc(bool, graph.size) catch @panic("Allocation error");
    defer al.free(visited);
    for (visited) |*v| v.* = false;
    
    // Lista de caminho
    var path = al.alloc(u32, graph.size * graph.size) catch @panic("Allocation error");
    defer al.free(path);
    var path_len: u32 = 0;
    
    var path_i: u32 = 0; // Indice do caminho
    var current_i: u32 = start; // vertice inicial
    var iter_count: u32 = 0; // numero de iteracoes
    while (iter_count < graph.size * graph.size) : (iter_count += 1) {
        // vertice atual foi visitado
        visited[current_i] = true;
        path[path_len] = current_i;
        path_len += 1;
        
        // Achar vertice mais proximo
        var shortest: u32 = current_i;
        {
            var shortest_value: i32 = math.maxInt(i32); // menor valor
            
            // achoar menor valor
            var i: u32 = 0;
            while (i < graph.size) : (i += 1) {
                if (i == current_i) continue;
                if (graph.flights[i + current_i * graph.size] == 0) continue;
                if (shortest == current_i) shortest = i;
                
                if (shortest_value > graph.flights[i + current_i * graph.size] and
                    !visited[i])
                {
                    shortest = i;
                    shortest_value = graph.flights[i + current_i * graph.size];
                }
            }
            
            // setar visinho
            if (visited[shortest]) {
                path_i -= 1;
                shortest = path[path_i];
            } else {
                path_i = path_len;
            }
        }
        
        // menor vira atual
        current_i = shortest;
        
        // verifica se todos foram visitados
        var all_visited = true;
        for (visited) |it| {
            if (!it) {
                all_visited = false;
                break;
            }
        }
        
        // se todos forem visitados sair do loop
        if (all_visited) break;
    }
    
    // Lista do caminho final
    var final_path = al.alloc(u32, path_len) catch unreachable;
    
    var pt: i32 = 0;
    for (path[0..path_len - 1]) |it, i| {
        pt += graph.flights[it + path[i + 1] * graph.size];
        final_path[i] = it;
    }
    final_path[path_len - 1] = path[path_len - 1];
    
    const end_time = time.nanoTimestamp();
    std.debug.print("world tour t = {}\n", .{end_time - init_time});
    
    // Retorna o caminho final
    return .{.price = pt, .path = final_path};
}

/// Chama o volta ao mundo comecando em todos os vertices e desenha o caminho de
/// menor preço
pub fn worldTourForce(al: *Allocator, graph: AeroportGraph) i32  {
    const init_time = time.nanoTimestamp();
    
    // Lista do caminho
    var path: []u32 = undefined;
    
    // preco
    var price: i32 = math.maxInt(i32);
    
    // acha menor caminho
    var j: u32 = 0;
    while (j < graph.size) : (j += 1) {
        var r = worldTour(al, graph, 0);
        if (r.price < price) {
            price = r.price;
            path = r.path;
        } else {
            al.free(r.path);
        }
    }
    
    // Desenha o caminho
    for (path[0..path.len - 1]) |it, i| {
        drawLineBetweenAeroports(graph.aeroports[it], graph.aeroports[path[i+1]] , -1, 0, 0);
    }
    draw.gb.drawBitmapFontFmt("Preco total = {}",.{price}, 10, draw.gb.height - 30, 2, 2, font_small);
    
    const result = path[0];
    
    // limpa memoria
    al.free(path);
    
    
    const end_time = time.nanoTimestamp();
    std.debug.print("world tour F t = {}\n", .{end_time - init_time});
    
    
    return @intCast(i32, result);
}

//check whether p is on the line or not
fn onLine(l1: [2][2]f32, p: [2]f32) bool { 
    const result =
        p[0] <= math.max(l1[0][0], l1[1][0]) and
        p[0] <= math.min(l1[0][0], l1[1][0]) and
        p[1] <= math.max(l1[0][1], l1[1][1]) and
        p[1] <= math.min(l1[0][1], l1[1][1]);
    
    return result;
}

fn direction(a: [2]f32, b: [2]f32, c: [2]f32) u32 {
    var val = (b[1] - a[1]) * (c[0] - b[0]) - (b[0] - a[0]) * (c[1] - b[1]);
    
    if (val == 0) return 0; //colinear
    if (val < 0) return 2; //anti-clockwise direction
    return 1; //clockwise direction
}

fn isIntersect(l1: [2][2]f32, l2: [2][2]f32) bool {
    //four direction for two lines and points of other line
    const dir1 = direction(l1[0], l1[1], l2[0]);
    const dir2 = direction(l1[0], l1[1], l2[1]);
    const dir3 = direction(l2[0], l2[1], l1[0]);
    const dir4 = direction(l2[0], l2[1], l1[1]);
    
    if(dir1 != dir2 and dir3 != dir4)
        return true; //they are intersecting
    
    return false;
}

// Grafo de intercessao
const ConnectionGraph = struct {
    size: u32 = 0,
    c: []i32,
    i: [][2]u32,
};

/// Cria um grafo de interceçao
pub fn createIntersectionGraph(al: *Allocator, a_graph: AeroportGraph) ConnectionGraph {
    var size: u32 = 0; // tamanho
    
    // Lista de intececao
    var intersect_list = al.alloc([2]u32, a_graph.size) catch unreachable;
    var intersect_index: u32 = 0;
    
    // Achar as intercessoes
    var j: u32 = 0;
    while (j < a_graph.size) : (j += 1) {
        var i: u32 = j + 1;
        while (i < a_graph.size) : (i += 1) {
            if (a_graph.flights[i + j * a_graph.size] == 0) continue;
            
            var j2: u32 = 0;
            while (j2 < a_graph.size) : (j2 += 1) {
                var i_2: u32 = j2 + 1;
                while (i_2 < a_graph.size) : (i_2 += 1) {
                    if (a_graph.flights[i_2 + j2 * a_graph.size] == 0) continue;
                    
                    // se o vertice for igual continua
                    if (i == i_2) continue;
                    if (i == j2) continue;
                    if (j == i_2) continue;
                    if (j == j2) continue;
                    
                    const l1 = [_][2]f32 {a_graph.aeroports[j].coords, a_graph.aeroports[i].coords};
                    const l2 = [_][2]f32 {a_graph.aeroports[j2].coords, a_graph.aeroports[i_2].coords};
                    
                    if (isIntersect(l1, l2)) {
                        size += 1;
                        intersect_list[intersect_index] = .{i, j};
                        intersect_index += 1;
                        break;
                    }
                }
            }
        }
    }
    
    // Resultado
    var result = ConnectionGraph {
        .size = size,
        .c = al.alloc(i32, size * size) catch unreachable,
        .i = undefined,
    };
    
    for (result.c) |*it| it.* = 0;
    
    // Cria o grafo de resultado
    for (intersect_list[0..intersect_index]) |it, i| {
        for (intersect_list[0..intersect_index]) |it2, i_2| {
            if (i_2 <= i) continue;
            
            if (it[0] == it2[0]) continue;
            if (it[0] == it2[1]) continue;
            if (it[1] == it2[0]) continue;
            if (it[1] == it2[1]) continue;
            
            const l1 = [_][2]f32 {a_graph.aeroports[it[0]].coords, a_graph.aeroports[it[1]].coords};
            const l2 = [_][2]f32 {a_graph.aeroports[it2[0]].coords, a_graph.aeroports[it2[1]].coords};
            
            if (isIntersect(l1, l2)) {
                // Desenha intercessao
                drawLineBetweenAeroports(a_graph.aeroports[it2[0]], a_graph.aeroports[it2[1]], -1, 0, 0);
                drawLineBetweenAeroports(a_graph.aeroports[it[0]], a_graph.aeroports[it[1]], -1, 0, 0);
                
                result.c[i + i_2 * size] = 1;
                result.c[i_2 + i * size] = 1;
            }
        }
    }
    
    result.i = intersect_list;
    
    return result;
}

/// Acha a altitue minima pra nao ter intercessao
pub fn setAltitude(al: *Allocator, graph: *AeroportGraph) void {
    const init_time = time.nanoTimestamp();
    
    // grafo de intercessao
    const intersect = createIntersectionGraph(al, graph.*);
    defer al.free(intersect.c);
    defer al.free(intersect.i);
    
    // lista de conexao
    var connection_count = al.alloc(u32, intersect.size) catch unreachable;
    defer al.free(connection_count);
    
    for (connection_count) |*it| it.* = 0;
    
    // Achar numero de conexoes
    var j: u32 = 0;
    while (j < intersect.size) : (j += 1) {
        var i: u32 = 0;
        while (i < intersect.size) : (i += 1) {
            if (i == j) continue;
            if (intersect.c[i + j * intersect.size] != 0) connection_count[j] += 1;
        }
    }
    
    // Ordem de visita
    var lookup_order = al.alloc(u32, intersect.size) catch unreachable;
    defer al.free(lookup_order);
    for (lookup_order) |*it, i| it.* = @intCast(u32, i);
    
    // Ordenar a ordem de visitas
    j = 0;
    while (j < intersect.size) : (j += 1) {
        var i: u32 = intersect.size - 1;
        while (i > j) : (i -= 1) {
            if (connection_count[lookup_order[i]] < connection_count[lookup_order[i - 1]]) {
                const tmp = lookup_order[i];
                lookup_order[i] = lookup_order[i - 1];
                lookup_order[i - 1] = tmp;
            }
        }
    }
    
    // reseta a altitude
    for (intersect.i[0..intersect.size]) |it| {
        graph.flights_height[it[0] + it[1] * graph.size] = 100;
        graph.flights_height[it[1] + it[0] * graph.size] = 100;
    }
    
    // Visita o grafo do menor para o maior
    for (lookup_order) |it| {
        var need_to_increment = false; // precisa incrementar
        
        // verifica se precisa incrementar
        var i: u32 = 0;
        while (i < intersect.size) : (i += 1) {
            if (i == it) continue;
            
            if (intersect.c[i + it * intersect.size] == 0) continue;
            
            const h = graph.flights_height[intersect.i[i][0] + intersect.i[i][1] * graph.size];
            if (connection_count[i] <= connection_count[it] and h == 100) {
                need_to_increment = true;
                break;
            }
        }
        
        // Incrementa a altitude
        if (need_to_increment) {
            const new_h = blk: {
                var result: i32 = 0;
                var k: u32 = 0;
                while (k < intersect.size) : (k += 1) {
                    if (k == it) continue;
                    
                    const h = graph.flights_height[intersect.i[k][0] + intersect.i[k][1] * graph.size];
                    if (h > result) {
                        result = h;
                    }
                }
                
                break :blk result;
            };
            
            graph.flights_height[intersect.i[it][0] + intersect.i[it][1] * graph.size] = new_h + 5;
            graph.flights_height[intersect.i[it][1] + intersect.i[it][0] * graph.size] = new_h + 5;
        }
    }
    
    // calcula o somatorio de altitude
    var t_h: i32 = 0;
    for (graph.flights_height) |it| {
        if (it != 0) t_h += it;
    }
    t_h = @divFloor(t_h, 20);
    
    
    const end_time = time.nanoTimestamp();
    std.debug.print("altitude t = {}\n", .{end_time - init_time});
    
    // desenha somatorio de altitude
    draw.gb.drawBitmapFontFmt("Altitude Total = {}k",.{t_h}, 10, draw.gb.height - 30, 2, 2, font_small);
}