//! Trabalho de grafos
//!
//! Esse é o arquivo principal do trabalho,
//! nele se encontra a função main e a interface do usuario
//!
//! O outro arquivo relevante é o graph.main

// === Import libs ===
const std = @import("std");
const Allocator = std.mem.Allocator;

const draw = @import("pixel_draw.zig");
const Texture = draw.Texture;
usingnamespace draw.vector_math;

usingnamespace @import("util.zig");
usingnamespace @import("graph.zig");

// Variaveis globais
var main_allocator: *Allocator = undefined; // Allocador de memoria
var mapa_mundi: Texture = undefined;   // Textura do mapa mundi
var moon_texture: Texture = undefined; // Textura da lua
var earth_mesh: draw.Mesh = undefined; // Mesh da terra
var moon_mesh: draw.Mesh = undefined;  // Mesh da lua

var main_graph: AeroportGraph = undefined; // O grafo principal do aeroporto

/// Funcao main
pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    main_allocator = &gpa.allocator;
    
    // Inicializar a biblioteca grafica
    // Essa função entra em looping
    try draw.init(main_allocator, 1280, 720, start, update);
    
    // Limpar memoria
    main_allocator.free(mapa_mundi.raw);
    main_allocator.free(font.texture.raw);
    main_allocator.free(font_small.texture.raw);
    
    freeAeroportGraph(main_allocator, &main_graph);
    
    main_allocator.free(earth_mesh.v);
    main_allocator.free(earth_mesh.i);
}

/// Função start, é chamada uma vez apos a inicialização grafica
fn start() void {
    
    // Carregar texturas
    mapa_mundi = textureFromEmbedTga("../assets/mundi.tga");
    moon_texture = textureFromEmbedTga("../assets/moon.tga");
    
    // Fontes para desenhar texto
    font = .{
        .texture = textureFromEmbedTga("../assets/font.tga"),
        .font_size_x = 12,
        .font_size_y = 16,
        .character_spacing = 12,
    };
    font_small = .{
        .texture = textureFromEmbedTga("../assets/font_small.tga"),
        .font_size_x = 10,
        .font_size_y = 12,
        .character_spacing = 8,
    };
    
    // Carrega o grafo dos arquivos de texto
    main_graph = createAeroportGraphFromFile(main_allocator) catch unreachable;
    
    // Cria meshes pra parte 3d
    earth_mesh = draw.createQuadMesh(main_allocator, 32, 32, 0, 0, mapa_mundi, .Strech);
    
    for (earth_mesh.v) |*v| {
        v.pos.x = (v.pos.x / 32.0) * -3.141592 * 2;
        v.pos.y = (v.pos.y / 32.0) * -3.141592;
        v.pos = sphericalToCartesian(10.0, v.pos.y, v.pos.x);
        v.pos = rotateVectorOnX(v.pos, -3.1415926535 * 0.5);
    }
    
    moon_mesh = draw.createQuadMesh(main_allocator, 32, 32, 0, 0, moon_texture, .Strech);
    for (moon_mesh.v) |*v| {
        v.pos.x = (v.pos.x / 32.0) * -3.141592 * 2;
        v.pos.y = (v.pos.y / 32.0) * -3.141592;
        v.pos = sphericalToCartesian(1.66, v.pos.y, v.pos.x);
        v.pos = rotateVectorOnX(v.pos, -3.1415926535 * 0.5);
    }
}

// Variaveis de utilidade
var cam: draw.Camera3D = .{ .pos = .{ .z = 20.0 }, .far = 100000 }; // Camera 3d
var mov_speed: f32 = 8.0; // velocidade de movimento da camera
var is_3d = false; // define se é pra desenhar em 2d ou 3d
var timer: f32 = 0.0; // Um timer

// Arrary com historico de frame time
var ms_hist: [64]f32 = [_]f32{0.016} ** 64;
var ms_hist_pos: u32 = 0;

// Variaveis relacionadas ao menu
var is_selecting_menu = false;
var is_menu_colapsed = false;
var menu_pos_x: i32 = 20;
var menu_pos_y: i32 = 400;

// Algoritimo selecionado
var selected_option: u32 = 0;

/// Função update, é chamada todo frame, delta é o tempo do frame
fn update(delta: f32) void {
    // Limpa a cor de fundo
    draw.gb.fillScreenWithRGBColor(0,0,0);
    
    // Troca entre 2d e 3d
    if (draw.keyDown(.r)) is_3d = !is_3d;
    
    if (is_3d) {
        // incrementa o timer
        timer += delta * 0.1;
        
        // le o teclado
        if (draw.keyPressed(.up)) cam.rotation.x += delta * 2;
        if (draw.keyPressed(.down)) cam.rotation.x -= delta * 2;
        if (draw.keyPressed(.right)) cam.rotation.y += delta * 2;
        if (draw.keyPressed(.left)) cam.rotation.y -= delta * 2;
        if (draw.keyPressed(._1)) cam.pos.y += delta * mov_speed;
        if (draw.keyPressed(._2)) cam.pos.y -= delta * mov_speed;
        if (draw.keyDown(._0)) mov_speed += 2.0;
        if (draw.keyDown(._9)) mov_speed -= 2.0;
        
        // Gira a camera
        var camera_forward = eulerAnglesToDirVector(cam.rotation);
        camera_forward.y = 0;
        var camera_right = eulerAnglesToDirVector(Vec3.c(cam.rotation.x, cam.rotation.y - 3.1415926535 * 0.5, cam.rotation.z));
        camera_right.y = 0;
        
        // le o movimento
        const input_z = draw.keyStrengh(.s) - draw.keyStrengh(.w);
        const input_x = draw.keyStrengh(.d) - draw.keyStrengh(.a);
        
        // Move a camera
        camera_forward = Vec3_mul_F(camera_forward, input_z);
        camera_right = Vec3_mul_F(camera_right, input_x);
        var camera_delta_p = Vec3_add(camera_forward, camera_right);
        camera_delta_p = Vec3_normalize(camera_delta_p);
        camera_delta_p = Vec3_mul_F(camera_delta_p, delta * mov_speed);
        cam.pos = Vec3_add(camera_delta_p, cam.pos);
        
        // Desenha a terra
        draw.gb.drawMesh(earth_mesh, .Texture, cam);
        
        // Move a lua
        var moon = draw.Mesh {
            .v = main_allocator.alloc(Vertex, moon_mesh.v.len) catch unreachable,
            .i = moon_mesh.i,
            .texture = moon_texture,
        };
        defer main_allocator.free(moon.v);
        
        for (moon.v) |*v, i| {
            v.* = moon_mesh.v[i];
            v.pos.x += @sin(timer) * 50;
            v.pos.z += @cos(timer) * 50;
        }
        
        // Desenha a lua
        draw.gb.drawMesh(moon, .Texture, cam);
        
        // Desenha os aeroportos
        for (main_graph.aeroports) |it| {
            const lat = (it.coords[0]) * 3.141592 / 180.0;
            const lon = (it.coords[1]) * 3.141592 / 180.0;
            
            var pos = Vec3 {
                .x = 10.1 * @cos(lat) * @cos(lon),
                .y = 10.1 * @cos(lat) * @sin(lon),
                .z = 10.1 * @sin(lat),
            };
            pos = rotateVectorOnX(pos, 3.141592 * 0.5);
            draw.gb.fillCircleIn3d(cam, pos, 0.2, Color.c(0.5, 0.5, 0.5, 1));
        }
    } else {
        // Parte 2d
        
        // Desenha a textura mapa mundi
        draw.gb.blitTexture(0, 0, mapa_mundi);
        
        // Seleciona os aeroportos
        for (main_graph.aeroports) |it, i| {
            // Posicao do mouse
            const dx = @intToFloat(f32, draw.mouse_pos_x - @intCast(i32, it.screen_pos[0]));
            const dy = @intToFloat(f32, draw.mouse_pos_y - @intCast(i32, it.screen_pos[1]));
            
            // Distancia do mouse para o aeroporto
            const dist = @sqrt(dx*dx + dy*dy);
            
            if (dist < 25) {
                if (draw.mouseButtonDown(.left)) {
                    selected_aeroport_1 = @intCast(i32, i);
                    if (selected_aeroport_2 == selected_aeroport_1) {
                        selected_aeroport_2 = -1;
                    }
                } else if (draw.mouseButtonDown(.right)) {
                    selected_aeroport_2 = @intCast(i32, i);
                    if (selected_aeroport_2 == selected_aeroport_1) {
                        selected_aeroport_1 = -1;
                    }
                }
            }
        }
        
        
        { // Desenha os voos
            var y: u32 = 0;
            while (y < main_graph.size) : (y += 1) {
                var x: u32 = 0;
                while (x < main_graph.size) : (x += 1) {
                    const f = main_graph.flights[x + y * main_graph.size];  // preco do voo
                    const fd = main_graph.flights_dist[x + y * main_graph.size]; // distancia do voo
                    const fh = main_graph.flights_height[x + y * main_graph.size]; // altitude do voo
                    
                    if (f != 0) {
                        const a1 = main_graph.aeroports[y]; // aeroporto 1
                        const a2 = main_graph.aeroports[x]; // aeroporto 2
                        drawLineBetweenAeroports(a1, a2, f, fd, fh); // desenha voo
                    }
                }
            }
        }
        
        // Desenha aeroportos
        for (main_graph.aeroports) |a| {
            drawAeroport(a);
        }
        
        // Seleciona opcoes
        if (draw.keyDown(._1)) {
            selected_option = 1;
        } else if (draw.keyDown(._2)) {
            selected_option = 2;
        } else if (draw.keyDown(._3)) {
            selected_option = 3;
        } else if (draw.keyDown(._4)) {
            selected_option = 4;
        }  else if (draw.keyDown(._5)) {
            selected_option = 5;
        } else if (draw.keyDown(._6)) {
            selected_option = 6;
        }  else if (draw.keyDown(._0)) {
            selected_option = 0;
        }
        
        // Roda os algoritimos
        switch (selected_option) {
            // Menor preço
            1 => {
                if (selected_aeroport_1 >= 0 and selected_aeroport_2 >= 0) {
                    shortestPath(main_allocator, main_graph,
                                 @intCast(u32, selected_aeroport_1),
                                 @intCast(u32, selected_aeroport_2),
                                 false);
                }
            },
            
            // Menor caminho
            2 => {
                if (selected_aeroport_1 >= 0 and selected_aeroport_2 >= 0) {
                    shortestPath(main_allocator, main_graph,
                                 @intCast(u32, selected_aeroport_1),
                                 @intCast(u32, selected_aeroport_2),
                                 true);
                }
            },
            
            // Altitudos
            3 => {
                setAltitude(main_allocator, &main_graph);
            },
            
            // Volta ao mundo
            4 => {
                if (selected_aeroport_1 >= 0) {
                    const r = worldTour(main_allocator, main_graph, @intCast(u32, selected_aeroport_1));
                    
                    for (r.path[0..r.path.len - 1]) |it, i| {
                        drawLineBetweenAeroports(main_graph.aeroports[it], main_graph.aeroports[r.path[i+1]] , -1, 0, 0);
                    }
                    
                    draw.gb.drawBitmapFontFmt("Preco total = {}",.{r.price}, 10, draw.gb.height - 30, 2, 2, font_small);
                    
                    main_allocator.free(r.path);
                }
            },
            
            // Volta ao mundo 2
            5 => {
                selected_aeroport_1 = worldTourForce(main_allocator, main_graph);
            },
            else => {},
        }
        
        
        // Desenhar o menu
        if ( menuHead("Algoritimos", &menu_pos_x, &menu_pos_y, &is_selecting_menu, &is_menu_colapsed) ) {
            var pos_x = menu_pos_x;
            var pos_y = menu_pos_y + 32;
            selectButton("Mais barato", 1, pos_x, &pos_y);
            selectButton("Menor Caminho", 2, pos_x, &pos_y);
            selectButton("Altitudes", 3, pos_x, &pos_y);
            selectButton("Volta ao mundo", 4, pos_x, &pos_y);
            selectButton("Volta ao mundo F", 5, pos_x, &pos_y);
        }
    }
    
    // Calcular fps
    ms_hist[ms_hist_pos] = delta;
    ms_hist_pos = (ms_hist_pos + 1) % 64;
    
    // Desenhar o fps
    if (draw.keyPressed(.q)) {
        var mdelta: f32 = 0.0;
        for (ms_hist) |it| mdelta += it;
        mdelta /= 64.0;
        
        draw.gb.drawBitmapFontFmt("{d:0.4} FPS / {d:0.2}ms",.{1 / mdelta, mdelta * 1000}, 10, 10, 1, 1, font);
        
        var x: u32 = 0;
        while (x < 63) : (x += 1) {
            var y = @floatToInt(i32, 200 - ms_hist[x] * 1000.0 * 4);
            var y2 = @floatToInt(i32, 200 - ms_hist[x + 1] * 1000.0 * 4);
            
            draw.gb.drawLine(@intCast(i32, x * 4 + 10), y,
                             @intCast(i32, (x + 1) * 4 + 10), y2,
                             Color.c(0, 0, 1, 1));
            
            draw.gb.drawLine(10, 200, 262, 200, Color.c(0,1,0,1));
            draw.gb.drawLine(10, 168, 262, 168, Color.c(0,1,0,1));
            draw.gb.drawLine(10, 136, 262, 136, Color.c(0,1,0,1));
            draw.gb.drawLine(10, 72, 262, 72, Color.c(0,1,0,1));
            draw.gb.drawBitmapFont("0 ms", 5, 201, 1, 1, font_small);
            draw.gb.drawBitmapFont("8 ms", 5, 169, 1, 1, font_small);
            draw.gb.drawBitmapFont("16 ms", 5, 137, 1, 1, font_small);
            draw.gb.drawBitmapFont("32 ms", 5, 73, 1, 1, font_small);
        }
    }
}

/// Cria o titulo do menu
fn menuHead(label: []const u8, x: *i32, y: *i32, is_selecting: *bool, is_colapsed: *bool) bool {
    const w = 196;
    const h = 32;
    
    const is_mouse_on = draw.mouse_pos_x >= x.* and draw.mouse_pos_y >= y.* and
        draw.mouse_pos_x < x.* + w and draw.mouse_pos_y < y.* + h;
    
    var fill_color = Color.c(0.4, 0.2, 0.2, 1.0);
    var border_color = Color.c(0.1, 0.1, 0.1, 1.0);
    
    if (is_mouse_on) {
        fill_color = Color.c(0.6, 0.2, 0.2, 1.0);
        
        if (draw.mouseButtonDown(.left)) {
            is_selecting.* = true;
        } else if (draw.mouseButtonUp(.left)) {
            is_selecting.* = false;
        }
        if (draw.mouseButtonUp(.right)) {
            is_colapsed.* = !is_colapsed.*;
        }
    }
    
    if (is_selecting.*) {
        x.* = draw.mouse_pos_x - 98;
        y.* = draw.mouse_pos_y - 16;
        
        fill_color = Color.c(0.8, 0.2, 0.2, 1.0);
    }
    
    draw.gb.fillRect(x.*, y.*, w, h, fill_color);
    draw.gb.drawRect(x.*, y.*, w, h, border_color);
    
    draw.gb.drawBitmapFont(label, @intCast(u32, x.*) + 2, @intCast(u32, y.*) + 8, 1, 1, font);
    
    return !is_colapsed.*;
}

/// Cria um botao
fn selectButton(label: []const u8, value: u32, x: i32, y: *i32) void {
    if(draw.doButton(&draw.gb, label, font, x, y.*, 196, 24, draw.mouse_pos_x, draw.mouse_pos_y, draw.mouseButtonPressed(.left), draw.mouseButtonUp(.left), selected_option == value))
    {
        if (selected_option == value) {
            selected_option = 0;
        } else {
            selected_option = value;
        }
    }
    y.* += 24;
}



/// Carrega textura
fn textureFromEmbedTga(comptime path: []const u8) draw.Texture {
    return draw.textureFromTgaData(main_allocator, @embedFile(path)) catch unreachable;
}
